# AutoComplete

![Screenshot](AutoComplete.png)

## How to run application

### GUI client
To run GUI client in dev mode please install [node.js](https://nodejs.org/en/) and then run following commands:
```
cd ./gui
npm install
bower install
grunt server
```

### Search Service

To run Search Service please install [sbt](http://www.scala-sbt.org/) and then run following commands:
```
cd ./service
sbt clean compile
sbt run
```

### Mongo Server

Mongo Server is distributed as a docker container with data saved in ```./mongo/``` directory.
To run Mongo Server type:
```
./mongo_create.sh
```

create and start container - ```./mongo_create.sh```
stop container - ```./mongo_stop.sh```
start container - ```./mongo_start.sh```
remove container = ```./.mongo_remove.sh```

MongoDb contains following documents
```
db.stores.insert(
   [
     { _id: 1, name: "Java Hut", description: "Coffee and cakes" },
     { _id: 2, name: "Burger Buns", description: "Gourmet hamburgers" },
     { _id: 3, name: "Coffee Shop", description: "Just coffee" },
     { _id: 4, name: "Clothes Clothes Clothes", description: "Discount clothing" },
     { _id: 5, name: "Java Shopping", description: "Indonesian goods" }
   ]
)
```

and index
```
db.stores.createIndex( { name: "text", description: "text" } )
```


## Assumptions


MongoDB full text search matches whole words only, so it is inherently not suitable for auto complete.

> The $text operator can search for words and phrases. The query matches on the complete stemmed words. For example, if a document field contains the word blueberry, a search on the term blue will not match the document. However, a search on either blueberry or blueberries will match.


(Source: http://docs.mongodb.org/manual/core/index-text/)

To solve this problem I decided to create Hint and Search services. The hint service looks for the key words which are shown in autocomplete combobox on gui site. The search service uses MongoDb text-search to query database and results are shown in the result section on gui site.

The Hint service as a argument receives name of the 'store' and query (part of th word). It returns word as a hole, left neighbour and word or word and right neighbor. 

## To do

* Think about better hint algorithm
* Think about optimization db queries
* Think about performance when more user will use application
* Check if code is not vulnerable to hacker attacks
* Consider to use Akka HTTP to create more reactive solution
* More tests (mainly in JavaScript)
* Use Docker to easy start whole application
* Rethink usage of MongoDb for full text search
* HintEvaluator as a strategy
* HintEvaluator doesn't work correctly for more than 1 word
* Fix logging
* Sometimes wrong part of sentence is bolded
* Change db from stores to products
* Remove from repository 'target' dir
* Find better solution to store pre-filled date in MongoDB

