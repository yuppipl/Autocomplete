$(function () {
    var url = "http://localhost:9000/v1";

    var table = $('#search_results').DataTable({
        "data": [],
        "searching": false
    });
    $("#search").autocomplete({
        minLength: 2,
        focus: function (event, ui) {
            $("#search").val(ui.item.value);
            return false;
        },
        source: function (request, response) {
            $.ajax({
                url: url + "/hint",
                dataType: "json",
                data: {
                    q: request.term
                },
                success: function (data) {
                    // Handle 'no match' indicated by [ "" ] response
                    var hints = [request.term].concat(data);
                    console.log(hints);
                    response(data.length === 1 && data[0].length === 0 ? [] : hints);
                }
            });
        },
        select: function (event, ui) {
            $.ajax({
                url: url + "/search",
                dataType: "json",
                data: {
                    q: ui.item.value
                },
                success: function (data) {
                    table.destroy();
                    table = $('#search_results').DataTable({
                        "data": data,
                        "searching": false,
                        columns: [
                            {data: "name"},
                            {data: "description"}
                        ]
                    });
                }
            });
        }
    }).autocomplete("instance")._renderItem = function (ul, item) {
        var searchValue = $("#search").val();
        var index = item.label.toLowerCase().indexOf(searchValue);
        console.log(index)
        return $("<li>")
                .append("<div>" + item.label.substring(0,index) + "<b>" + item.label.substring(index,index+searchValue.length+1) + "</b>" + item.label.substring(index+searchValue.length+1) + "</div>")
                .appendTo(ul);
    };
});
