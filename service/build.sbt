name          := "Autocomplete"
organization  := "pl.wegrzynski"
version       := "0.0.1"
scalaVersion  := "2.11.8"
scalacOptions := Seq("-unchecked", "-feature", "-deprecation", "-encoding", "utf8")

resolvers += Resolver.jcenterRepo
resolvers += "Sonatype OSS Releases" at "https://oss.sonatype.org/content/repositories/releases"
resolvers += "Sonatype OSS Snapshots" at "https://oss.sonatype.org/content/repositories/snapshots"

libraryDependencies ++= {
  val scalazV          = "7.3.0-M2"
  val akkaV            = "2.4.7"
  val ficusV           = "1.2.4"
  val scalaTestV       = "3.0.0-M15"
  val scalaMockV       = "3.2.2"
  val scalazScalaTestV = "0.3.0"
  val mongoDriverV     = "1.1.1"
  val casbahV          = "3.1.1"
  val akkaCorsV        = "0.1.5"

  Seq(
    "com.typesafe.akka" %% "akka-http-core"                    % akkaV,
    "com.typesafe.akka" %% "akka-http-experimental"            % akkaV,
    "com.typesafe.akka" %% "akka-http-spray-json-experimental" % akkaV,
    "com.iheart"        %% "ficus"                             % ficusV,
    "org.scalatest"     %% "scalatest"                         % scalaTestV       % "it,test",
    "org.scalamock"     %% "scalamock-scalatest-support"       % scalaMockV       % "it,test",
    "org.typelevel"     %% "scalaz-scalatest"                  % scalazScalaTestV % "it,test",
    "com.typesafe.akka" %% "akka-http-testkit"                 % akkaV            % "it,test",
    "org.mongodb.scala" %% "mongo-scala-driver"                % mongoDriverV,
    "org.mongodb"       %% "casbah-core"                       % casbahV,
    "ch.megard"         %% "akka-http-cors"                    % akkaCorsV
  )
}

lazy val root = project.in(file(".")).configs(IntegrationTest)
Defaults.itSettings
Revolver.settings
enablePlugins(JavaAppPackaging)
