package pl.wegrzynski

object HintEvaluator {

  def createHints(sentence: String, query: String): Set[String] = {
    def createHintsWithLowerCase(sentence: String, query: String): Set[String] = {
      def findNeighbours(list: List[String], value: String): (Option[String], Option[String], Option[String]) = {
        def element(i: Int): Option[String] =
          if (i >= 0 && i < list.size) Some(list(i).trim) else None

        val index = list.indexOf(value)
        (element(index - 1), element(index), element(index + 1))
      }

      def combineNeighbours(friends: (Option[String], Option[String], Option[String])): List[String] = {
        def toString(list: List[Option[String]]) = list.map(_.get)
        def filterOutNone(list: List[Option[String]]) = list.filter {
          case Some(x) => true
          case _ => false
        }
        def print(list: List[Option[String]]) = toString(filterOutNone(list)).mkString(" ")
        val elements = List(List(friends._2), List(friends._1, friends._2), List(friends._2, friends._3))
        elements.map(print)
      }

      def indexOfLeft(value: String) = {
        val index: Int = value.lastIndexOf(" ")
        if (index >= 0) index else 0
      }

      def indexOfRight(value: String) = {
        val index: Int = value.indexOf(" ")
        if (index >= 0) index else value.length
      }

      val indexLeft = indexOfLeft(sentence.substring(0, sentence.indexOf(query)))
      val indexQueryRight: Int = sentence.indexOf(query) + query.length
      val indexRight = indexQueryRight + indexOfRight(sentence.substring(indexQueryRight))
      val left = sentence.substring(0, indexLeft).trim
      val right = sentence.substring(indexRight).trim
      val center = sentence.substring(indexLeft, indexRight).trim

      val words = (left.trim.split(" ").toList ++ List(center) ++ right.trim.split(" ")).filterNot(_.isEmpty)
      val tokens = words.filter(_.toLowerCase().contains(query.toLowerCase()))
      tokens.flatMap(x => combineNeighbours(findNeighbours(words, x))).toSet
    }
    createHintsWithLowerCase(sentence.toLowerCase(), query.toLowerCase())
  }

}
