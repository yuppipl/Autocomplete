package pl.wegrzynski

import akka.http.scaladsl.server.Directives._
import ch.megard.akka.http.cors.CorsDirectives._
import com.mongodb.casbah.Imports._
import com.mongodb.casbah.MongoClient
import com.mongodb.{ BasicDBObject, DBObject }

trait SearchService extends BaseService {
  val stores = {
    val mongoClient = MongoClient(mongoConfig.host, mongoConfig.port)
    val db = mongoClient(mongoConfig.db)
    db(mongoConfig.collection)
  }

  protected val statusRoutes =
    pathPrefix("v1") {
      cors() {
        path("search") {
          (get & parameters('q.as[String])) { (query: String) =>
            val mongoSearchQuery: DBObject = $text(query).$language(searchQuery.language)
            val mongoScoreFields: DBObject = new BasicDBObject("score", new BasicDBObject("$meta", "textScore"))
            val sortCriteria: DBObject = mongoScoreFields
            val find = stores.find(mongoSearchQuery, mongoScoreFields).sort(sortCriteria)
            val result: List[Item] = find.map((dbObject: DBObject) => {
              Item(dbObject.get("name").toString, dbObject.get("description").toString)
            }).toList
            complete(result)
          }
        } ~
          path("hint") {
            (get & parameters('q.as[String])) { (query: String) =>
              val find = stores.find(new BasicDBObject("name", new BasicDBObject("$regex", query) ++ new BasicDBObject("$options", "i")))
              val result: Set[String] = find.flatMap((dBObject: DBObject) => {
                HintEvaluator.createHints(dBObject.get("name").toString, query)
              }).toSet
              complete(result)
            }
          }
      }
    }
}

