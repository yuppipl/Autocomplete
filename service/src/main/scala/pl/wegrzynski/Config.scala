package pl.wegrzynski

import com.typesafe.config.ConfigFactory
import net.ceedubs.ficus.Ficus
import net.ceedubs.ficus.readers.ArbitraryTypeReader

trait Config {

  import ArbitraryTypeReader._
  import Ficus._

  protected case class HttpConfig(interface: String, port: Int)

  protected case class SearchQuery(language: String)

  protected case class MongoConfig(host: String, port: Int, db: String, collection: String)

  private val config = ConfigFactory.load()
  protected val httpConfig = config.as[HttpConfig]("http")
  protected val searchQuery = config.as[SearchQuery]("search_query")
  protected val mongoConfig = config.as[MongoConfig]("mongo")
}
