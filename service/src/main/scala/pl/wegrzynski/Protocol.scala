package pl.wegrzynski

import spray.json.DefaultJsonProtocol

case class Item(name: String, description: String)

trait Protocol extends DefaultJsonProtocol {
  implicit val itemFormatter = jsonFormat2(Item.apply)
}
