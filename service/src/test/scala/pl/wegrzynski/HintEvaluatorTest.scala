package pl.wegrzynski

import org.scalatest.{ FlatSpec, Matchers }

class HintEvaluatorTest extends FlatSpec with Matchers {

  "one word query for sentence where query is the same as beggingi of sentence" should "return following hints" in {
    val hints: Set[String] = HintEvaluator.createHints("Java", "ja")
    hints should equal(Set("java"))
  }

  "one word query for sentence where query appears twice" should "return following hints" in {
    val hints: Set[String] = HintEvaluator.createHints("Caffee drink java giraffe", "affe")
    hints should equal(Set("caffee", "caffee drink", "giraffe", "java giraffe"))
  }

  "one word query or sentence where query appears once without neighbours" should "return following hints" in {
    val hints: Set[String] = HintEvaluator.createHints("Caffee", "affe")
    hints should equal(Set("caffee"))
  }

  "one word query for sentence where query appears once with left neighbour" should "return following hints" in {
    val hints: Set[String] = HintEvaluator.createHints("left House Caffee", "affe")
    hints should equal(Set("caffee", "house caffee"))
  }

  "one word query for sentence where query appears once with right neighbour" should "return following hints" in {
    val hints: Set[String] = HintEvaluator.createHints("Caffee shop right", "affe")
    hints should equal(Set("caffee", "caffee shop"))
  }

  "one word query for sentence where query appears once with both neighbours" should "return following hints" in {
    val hints: Set[String] = HintEvaluator.createHints("left shop Caffee house right", "affe")
    hints should equal(Set("caffee", "shop caffee", "caffee house"))
  }

  "one word query for sentence where query appears a lot of times" should "return following hints" in {
    val hints: Set[String] = HintEvaluator.createHints("Clothes Clothes Clothes", "lot")
    hints should equal(Set("clothes", "clothes clothes"))
  }

  "two words query for sentence where query appears once without neighbours" should "return following hints" in {
    val hints: Set[String] = HintEvaluator.createHints("Discount clothing", "nt cl")
    hints should equal(Set("discount clothing"))
  }

  "two words query for sentence where query is equal to sentence" should "return following hints" in {
    val hints: Set[String] = HintEvaluator.createHints("Discount clothing", "Discount clothing")
    hints should equal(Set("discount clothing"))
  }

  "two words query for sentence where query appears once with left neighbour" should "return following hints" in {
    val hints: Set[String] = HintEvaluator.createHints("Coffee Discount clothing", "nt cl")
    hints should equal(Set("discount clothing", "coffee discount clothing"))
  }

}
