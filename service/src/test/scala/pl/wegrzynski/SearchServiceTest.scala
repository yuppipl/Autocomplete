package pl.wegrzynski

import akka.http.scaladsl.model.StatusCodes

/**
 * Test requires started and configured MongoDb. It could be done by running ./mongo_create.sh
 */
class SearchServiceTest extends ServiceTestBase with SearchService {
  "SearchService" when {

    "GET /v1/search" should {
      "return seq of items" in {
        Get("/v1/search?q=java") ~> statusRoutes ~> check {
          status should be(StatusCodes.OK)
          responseAs[List[Item]] should equal(List(Item("Java Hut", "Coffee and cakes"), Item("Java Shopping", "Indonesian goods")))
        }
      }
    }

    "GET /v1/hint" should {
      "return hints" in {
        Get("/v1/hint?q=java") ~> statusRoutes ~> check {
          status should be(StatusCodes.OK)
          responseAs[List[String]] should equal(List("Java", "Java Hut", "Java Shopping"))
        }
      }
    }
  }
}
